#!/bin/bash
a=$(ls | grep "index.html")
if [ ! -e index.html ] 
then
		echo "index.html do not exists"
		exit 0:
else
		#cat index.html | sed ':a;N;$!ba;s/\n/@@/g'

		head=$(cat index.html | sed -i '' -e ':a;N;$!ba;s/\n/@@/g' | grep -o "<head>.*<\/head>")
		header=$(cat index.html | sed -i '' -e ':a;N;$!ba;s/\n/@@/g' | grep -o "<header>.*<\/header>")
		footer=$(cat index.html | sed -i '' -e ':a;N;$!ba;s/\n/@@/g' | grep -o "<footer>.*<\/footer>")
		
    
fi
if [ "$1" = "-r" ] || [ "$1" = "--recursive" ]
then
    	files=$(find . -name "*.html")
    	#echo $files
else
    	files=$(ls *.html)
    	#echo $files
fi
for file in $files
do
		touch @@@
		cat $file | sed -i '' -e ':a;N;$!ba;s/\n/@@/g' > @@@
		head1=$(grep -o "<head>.*<\/head>" @@@)
	    header1=$(grep -o "<header>.*<\/header>" @@@)
	    footer1=$(grep -o "<footer>.*<\/footer>" @@@)
		if [ ! -z "$head1" ]
		then
		    sed -i '' -e "s|$head1|$head|g" @@@
		else
	        sed -i '' -e "s|<html>|<html>$head|g" @@@
		fi

	    if [ ! -z "$header1" ]
		then
		    sed -i '' -e "s|$header1|$header|g" @@@
		else
	        sed -i '' -e "s|<body>|<body>$header|g" @@@
		fi

	    if [ ! -z "$footer1" ]
		then
		    sed -i '' -e "s|$footer1|$footer|g" @@@
		else
	        sed -i '' -e "s|<\/body>|$footer<\/body>|g" @@@
		fi

		sed -i '' -e 's|@@|\n|g' @@@
		cat @@@ > $file
		rm @@@ 
    
done